var os = require('os');

console.log('Tu Sistema operativo es '+ os.type()); // "Windows_NT"
console.log('Tu version es '+ os.release()); // "10.0.14393"
console.log('La plataforma es '+os.platform()); 
console.log('Tu arquitectura '+os.arch()); 
console.log('Memoria libre '+ os.freemem() + ' de ' + os.totalmem()); 
console.log('Tu CPU es ' + os.cpus()); 
console.log('La compu ha vivido por '+os.uptime()+' segundos'); 
console.log('El lugar donde se instalan tus porgramas -->  '+os.homedir()); 