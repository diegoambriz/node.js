const express = require('express')
const app = express()
const port = 3000
const mongoose = require('mongoose')
const mongoConfig = { useNewUrlParser: true}
const bodyParser = require('body-parser')
const cors = require('./middlewares/cors')

mongoose.connect('mongodb://localhost:27017/test', mongoConfig);

app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())
app.set('views', './views')
app.set('view engine', 'pug')
app.use(express.static('public'))

//Cors 
app.use(cors)


app.get('/', function (req, res) {
    res.render('./pages/index')
})
// => /felicidades/saul/toña
// <="Hola toña!, muchas felicidades, De Diego"
app.get('/felicidades/:de/:para', (req,res)=>{
    const de = req.params.de
    const para = req.params.para
    res.send('Hola '+ para +'muchas felicidades, de ' + de)
})

app.get('/zoo/:animal/:sonido',(req,res)=> {
    const animal = req.params.animal
    const sonido = req.params.sonido
    res.send('El '+ animal +' hace ' + sonido)
})
// => /zoo/cerdo/oink
// <= "El cerdo hace oink!"
const rutas = require('./rutas')
app.use('/', rutas)
app.listen(port,()=>console.log(`Example app listening on port ${port}!`))
