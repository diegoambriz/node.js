function cors (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD, OPTIONS, POST, PUT');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    next();
}

module.exports = cors
