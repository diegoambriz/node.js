const mongoose = require('mongoose')
var Personschema = new mongoose.Schema({
    nombre: {
        type: String,
        required: false

    },
    apellido: {
        type: String,
        required: false
    },
    edad:{
        type: Number,
        required: false
    },
    casado:{
        type:Boolean,
        required: false
    }
})
var modelPersonas = mongoose.model('User', Personschema)
module.exports = modelPersonas